<?php

namespace Gabelbart\ContaoSimpleOsmBundle\ContaoManager;

use Gabelbart\ContaoSimpleOsmBundle\ContaoSimpleOsmBundle;

class Plugin implements \Contao\ManagerPlugin\Bundle\BundlePluginInterface
{
    public function getBundles(\Contao\ManagerPlugin\Bundle\Parser\ParserInterface $parser): array
    {
        return [
            \Contao\ManagerPlugin\Bundle\Config\BundleConfig::create(ContaoSimpleOsmBundle::class)
                ->setLoadAfter([
                    \Contao\CoreBundle\ContaoCoreBundle::class,
                    ContaoSimpleOsmBundle::ContaoCookiebarClass
                ])
                ->setReplace(['contao-simple-osm']),
        ];
    }
}
