<?php

$GLOBALS['TL_DCA']['tl_module']['palettes']['openstreetmap_iframe'] =
    '{title_legend},name,headline,type;' .
    '{config_legend},osm_width,osm_height,osm_center_lat,osm_center_lng,osm_zoom,osm_type,osm_link,osm_marker;' .
    '{template_legend:hide},customTpl;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests,cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'osm_marker' ;
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['osm_marker'] = 'osm_marker_lat,osm_marker_lng' ;


$GLOBALS['TL_DCA']['tl_module']['fields']['osm_center_lng'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_center_lng'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class' => 'w50', 'mandatory'=>true, 'min' => -180, 'max' => 180),
    'sql'                     => "varchar(20) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['osm_center_lat'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_center_lat'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class' => 'w50', 'mandatory'=>true, 'min' => -90, 'max' => 90),
    'sql'                     => "varchar(20) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['osm_marker_lng'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_marker_lng'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class' => 'w50', 'mandatory'=>true, 'min' => -180, 'max' => 180),
    'sql'                     => "varchar(20) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['osm_marker_lat'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_marker_lat'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class' => 'w50', 'mandatory'=>true, 'min' => -90, 'max' => 90),
    'sql'                     => "varchar(20) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['osm_width'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_width'],
    'exclude'                 => true,
    'inputType'               => 'inputUnit',
    'options'                 => [$GLOBALS['TL_CSS_UNITS'][0]],
    'eval'                    => array('tl_class' => 'w50', 'mandatory'=>true, 'rgxp' => 'digit'),
    'sql'                     => "varchar(100) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['osm_height'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_height'],
    'exclude'                 => true,
    'inputType'               => 'inputUnit',
    'options'                 => [$GLOBALS['TL_CSS_UNITS'][0]],
    'eval'                    => array('tl_class' => 'w50', 'mandatory'=>true, 'rgxp' => 'digit'),
    'sql'                     => "varchar(100) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['osm_zoom'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_zoom'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class' => 'w50', 'mandatory'=>true, 'min' => 1, 'max' => 20),
    'sql'                     => "int(2) NOT NULL default '10'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['osm_type'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_type'],
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'				=> &$GLOBALS['TL_LANG']['tl_module']['osm_type_options'],
    'eval'                    => array('tl_class' => 'w50', 'mandatory'=>true),
    'sql'                     => "varchar(14) NOT NULL default 'mapnik'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['osm_marker'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_marker'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class' => 'w50 m12', 'submitOnChange' => true),
    'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['osm_link'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['osm_link'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class' => 'w50 m12'),
    'sql'                     => "char(1) NOT NULL default ''"
);
