<?php

$GLOBALS['TL_LANG']['tl_module']['osm_width'] = ['Kartenbreite', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_height'] = ['Kartenhöhe', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_center_lat'] = ['Kartenmittelpunkt (Breite)', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_center_lng'] = ['Kartenmittelpunkt (Länge)', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_marker_lat'] = ['Markerposition (Breite)', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_marker_lng'] = ['Markerposition (Länge)', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_marker'] = ['Karten-Marker', 'Einen Marker auf der Karte zeigen'];
$GLOBALS['TL_LANG']['tl_module']['osm_zoom'] = ['Kartenzoom', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_type'] = ['Kartentyp', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_link'] = ['Detaillink', 'Einen Link zum öffnen im neuen Fenster zeigen'];
$GLOBALS['TL_LANG']['tl_module']['osm_type_options'] = [
    'mapnik' => 'Mapnik',
    'cyclemap' => 'Radkarte',
    'transportmap' => 'Transport',
    'hot' => 'Heiß?!'
];
