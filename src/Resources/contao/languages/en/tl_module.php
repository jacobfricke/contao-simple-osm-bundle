<?php

$GLOBALS['TL_LANG']['tl_module']['osm_width'] = ['Map width', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_height'] = ['Map height', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_center_lat'] = ['Map center (latitude)', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_center_lon'] = ['Map Center (longitude)', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_marker_lat'] = ['Marker position (latitude)', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_marker_lon'] = ['Marker position (longitude)', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_marker'] = ['Map marker', 'Show a marker on the map'];
$GLOBALS['TL_LANG']['tl_module']['osm_zoom'] = ['Map zoom', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_type'] = ['Map type', ''];
$GLOBALS['TL_LANG']['tl_module']['osm_link'] = ['Detail link', 'A link to open the map in a new window'];
$GLOBALS['TL_LANG']['tl_module']['osm_type_options'] = [
    'mapnik' => 'Mapnik',
    'cyclemap' => 'Radkarte',
    'transportmap' => 'Transport',
    'hot' => 'Heiß?!'
];
