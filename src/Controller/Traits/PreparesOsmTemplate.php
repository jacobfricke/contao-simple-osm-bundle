<?php

namespace Gabelbart\ContaoSimpleOsmBundle\Controller\Traits;

use Contao\ModuleModel;

/**
 * @mixin \Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController
 */
trait PreparesOsmTemplate
{
    /**
     * @return void
     */
    protected function prepareGlobals(): void
    {
        $GLOBALS['TL_CSS'][] = 'bundles/contaosimpleosm/style.css|static';
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return mixed
     */
    protected function getHeight(ModuleModel $model): mixed
    {
        return unserialize($model->osm_height);
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return mixed
     */
    protected function getWidth(ModuleModel $model): mixed
    {
        return unserialize($model->osm_width);
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return float
     */
    protected function getCenterLat(ModuleModel $model): float
    {
        return floatval($model->osm_center_lat);
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return float
     */
    protected function getCenterLng(ModuleModel $model): float
    {
        return floatval($model->osm_center_lng);
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return ?int
     */
    protected function getZoom(ModuleModel $model): ?int
    {
        return intval($model->osm_zoom);
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return bool
     */
    protected function hasMarker(ModuleModel $model): bool
    {
        return $model->osm_marker === '1';
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return bool
     */
    protected function showMapLink(ModuleModel $model): bool
    {
        return $model->osm_link === '1';
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return ?float
     */
    protected function getMarkerLat(ModuleModel $model): ?float
    {
        return floatval($model->osm_marker_lat);
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return string
     */
    protected function getLayerType(ModuleModel $model): string
    {
        return $model->osm_type;
    }

    /**
     * @param \Contao\ModuleModel $model
     * @return mixed|null
     */
    protected function getMarkerLng(ModuleModel $model): float
    {
        return floatval($model->osm_marker_lng);
    }
}
