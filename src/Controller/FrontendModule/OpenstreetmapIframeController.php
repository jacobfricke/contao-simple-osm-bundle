<?php

namespace Gabelbart\ContaoSimpleOsmBundle\Controller\FrontendModule;

use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\ModuleModel;
use Contao\Template;

use Gabelbart\ContaoSimpleOsmBundle\Openstreetmap\OpenstreetmapIframe;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Gabelbart\ContaoSimpleOsmBundle\Openstreetmap\OpenstreetmapLink;
use Gabelbart\ContaoSimpleOsmBundle\Controller\Traits\PreparesOsmTemplate;

/**
 * @FrontendModule(category="simple-osm")
 */
class OpenstreetmapIframeController extends AbstractFrontendModuleController
{
    use PreparesOsmTemplate;

    protected function getResponse(Template $template, ModuleModel $model, Request $request): ?Response
    {
        $this->prepareGlobals();

        $centerLat = $this->getCenterLat($model);
        $centerLon = $this->getCenterLng($model);
        $zoom = $this->getZoom($model);

        $hasMarker = $this->hasMarker($model);
        $markerLat = $this->getMarkerLat($model);
        $markerLng = $this->getMarkerLng($model);
        $layerType = $this->getLayerType($model);

        $width = $this->getWidth($model);
        $height = $this->getHeight($model);

        $iframeUrl = OpenstreetmapIframe::make($centerLat, $centerLon, $zoom)
            ->layerType($layerType)
            ->withMarker($hasMarker,  floatval($markerLat), floatval($markerLng));
        if ($width['unit'] === 'px') {
            $iframeUrl->width($width['value']);
        }
        if ($height['unit'] === 'px') {
            $iframeUrl->height($height['value']);
        }

        $mapLink = OpenstreetmapLink::make($centerLon, $centerLat, $zoom)
            ->layerType($layerType);
        if ($hasMarker) {
            $mapLink->withMarker($hasMarker, $markerLat, $markerLng);
        }

        $template->iframe = $iframeUrl->toUrl();

        $template->maplink = $mapLink->toUrl();
        $template->displayLink = $this->showMapLink($model);

        $template->width = $width;
        $template->height = $height;

        return $template->getResponse();
    }
}
