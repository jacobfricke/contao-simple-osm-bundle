<?php

namespace Gabelbart\ContaoSimpleOsmBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

use Gabelbart\ContaoSimpleOsmBundle\ContaoSimpleOsmBundle;

class ContaoSimpleOsmExtension extends Extension
{
    public function load(array $mergedConfig, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yml');
        if (ContaoSimpleOsmBundle::contaoCookiebarIsInstalled()) {
            $iframeTypes = $container->getParameter('contao_cookiebar.iframe_types') ?? [];
            $iframeTypes['contao_simpleosm'] = ['mod_openstreetmap_iframe'];
            $container->setParameter('contao_cookiebar.iframe_types', $iframeTypes);
        }
    }
}
