<?php

namespace Gabelbart\ContaoSimpleOsmBundle\Openstreetmap;

class UrlBuilder
{
    protected const REG_QUERY_ARRAY_KEY = '/(.+)\[([^]]*)\]/i';
    protected array $query = [];
    protected ?string $hash = null;

    public function __construct(protected string $base)
    {
    }

    public static function make(string $base): static
    {
        return new static($base);
    }

    public function hash(?string $hash = null): static
    {
        $this->hash = $hash;

        return $this;
    }

    public function query(string $key, string $value): static
    {
        $isArray = 1 === preg_match(static::REG_QUERY_ARRAY_KEY, $key, $matches);
        return $isArray
            ? $this->addQueryAsArray($matches[1], $value, $matches[2])
            : $this->addQuery($key, $value);
    }

    public function addQuery(string $key, string $value): static
    {
        $this->query[$key] = $value;

        return $this;
    }

    public function addQueryAsArray(string $key, string $value, string $index = ''): static
    {
        if (!is_array($this->query[$key])) {
            $this->query[$key] = [];
        }

        if (strlen($index) === 0) {
            $this->query[$key][] = $value;
        } else {
            $this->query[$key][$index] = $value;
        }

        return $this;
    }

    public function toUrl(): string
    {
        $link = $this->base;
        if (count($this->query) > 0) {
            $link .= $this->encodeQuery();
        }

        if (!is_null($this->hash)) {
            $link .= "#{$this->hash}";
        }

        return $link;
    }

    public function __toString(): string
    {
        return $this->toUrl();
    }

    protected function encodeQuery(): string
    {
        $query = array_map(
            function($value, $key) {
                $key = urlencode($key);
                if (is_array($value)) {
                    return $this->encodeQueryArray($value, $key);
                }
                return "$key=" . urlencode($value);
            },
            array_values($this->query),
            array_keys($this->query)
        );
        return '?' . join('&', $query);
    }

    /**
     * @param array $value
     * @param string $key
     * @return string
     */
    protected function encodeQueryArray(array $value, string $key): string
    {
        $queryPart = [];
        foreach ($value as $subKey => $item) {
            $item = urlencode($item);
            if (is_string($subKey) && strlen($subKey) > 0) {
                $subKey = urlencode($subKey);
                $queryPart[] = "{$key}[{$subKey}]={$item}";
            }
        }
        return join('&', $queryPart);
    }
}
