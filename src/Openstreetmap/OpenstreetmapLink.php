<?php

namespace Gabelbart\ContaoSimpleOsmBundle\Openstreetmap;

class OpenstreetmapLink
{
    /**
     * Base URL for the link
     * @var string
     */
    protected const linkUrl = 'https://www.openstreetmap.org/';

    /**
     * URL hash for link location settings
     * Parameters: zoom; lat; lng
     * @var string
     */
    protected const hashLocation = 'map=%s/%s/%s';

    /**
     * Query name for marker latitude position
     * @var string
     */
    protected const queryMarkerLat = 'mlat';

    /**
     * Query name for marker longitude position
     * @var string
     */
    protected const queryMarkerLon = 'mlon';

    /**
     * Query name for layer type
     * @var string
     */
    protected const queryLayers = 'layers';

    protected bool $withMarker = true;
    protected ?float $markerLat = null;
    protected ?float $markerLon = null;
    protected ?string $layerType = null;

    public function __construct(
        protected float $lat,
        protected float $lon,
        protected int $zoom
    )
    {
    }

    public static function make(float $lat, float $lon, int $zoom = 10): static
    {
        return new static($lat, $lon, $zoom);
    }

    public function layerType(string $layerType): static
    {
        $this->layerType = $layerType;

        return $this;
    }

    public function withMarker(bool $withMarker, ?float $lat = null, ?float $lon = null): static
    {
        $this->withMarker = $withMarker;

        if (!(empty($lat) || empty($lon))) {
            return $this->markerPosition($lat, $lon);
        }

        return $this;
    }

    public function markerPosition(float $lat, float $lon): static
    {
        $this->markerLat = $lat;
        $this->markerLon = $lon;

        return $this;
    }

    public function toUrl(): string
    {
        $link = UrlBuilder::make(static::linkUrl)
            ->hash(sprintf(static::hashLocation, $this->zoom, $this->lat, $this->lon));

        if ($this->withMarker) {
            $link->query(static::queryMarkerLat, $this->markerLat ?? $this->lat);
            $link->query(static::queryMarkerLon, $this->markerLon ?? $this->lon);
        }

        switch ($this->layerType) {
            case 'cyclemap':
                $link->query(static::queryLayers, 'C');
                break;
            case 'transportmap':
                $link->query(static::queryLayers, 'T');
                break;
            case 'hot':
                $link->query(static::queryLayers, 'H');
                break;
        }

        return $link->toUrl();
    }

    public function __toString(): string
    {
        return $this->toUrl();
    }
}
