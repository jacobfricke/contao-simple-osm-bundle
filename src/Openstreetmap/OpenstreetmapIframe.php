<?php

namespace Gabelbart\ContaoSimpleOsmBundle\Openstreetmap;

class OpenstreetmapIframe
{
    /**
     * Size of the map tiles in px
     */
    protected const tileSize = 265;

    /**
     * Base URL for the link
     * @var string
     */
    protected const embedUrl = 'https://www.openstreetmap.org/export/embed.html';

    /**
     * Query name for map bounding box
     * @var string
     */
    protected const queryBoundingBox = 'bbox';

    /**
     * Query name for marker latitude position
     * @var string
     */
    protected const queryBoundingBoxPattern = '%s,%s,%s,%s';

    /**
     * Query name for marker position
     * @var string
     */
    protected const queryMarker = 'marker';

    /**
     * Query value pattern for marker position
     * @var string
     */
    protected const queryMarkerPattern = '%s,%s';

    /**
     * Query name for layer type
     * @var string
     */
    protected const queryLayer = 'layer';

    protected bool $withMarker = true;
    protected ?float $markerLat = null;
    protected ?float $markerLon = null;
    protected ?string $layerType = null;

    // Width/height are needed for the calculation of the bounding box
    protected int $width = 450;
    protected int $height = 450;

    public function __construct(
        protected float $lat,
        protected float $lon,
        protected int $zoom
    )
    {
    }

    public static function make(float $lat, float $lon, int $zoom = 10): static
    {
        return new static($lat, $lon, $zoom);
    }

    public function layerType(string $layerType): static
    {
        $this->layerType = $layerType;

        return $this;
    }

    public function withMarker(bool $withMarker, ?float $lat = null, ?float $lon = null): static
    {
        $this->withMarker = $withMarker;

        if (!(empty($lat) || empty($lon))) {
            return $this->markerPosition($lat, $lon);
        }

        return $this;
    }

    public function width(int $width): static
    {
        $this->width = $width;

        return $this;
    }

    public function height(int $height): static
    {
        $this->height = $height;

        return $this;
    }

    public function markerPosition(float $lat, float $lon): static
    {
        $this->markerLat = $lat;
        $this->markerLon = $lon;

        return $this;
    }

    public function toUrl(): string
    {
        $link = UrlBuilder::make(static::embedUrl);

        $link->query(
            static::queryBoundingBox,
            $this->getBoundingBoxQueryValue());

        if ($this->withMarker) {
            $link->query(
                static::queryMarker,
                sprintf(static::queryMarkerPattern,
                    $this->markerLat ?? $this->lat,
                    $this->markerLon ?? $this->lon));
        }

        if (!empty($this->layerType)) {
            $link->query(static::queryLayer, $this->layerType);
        }

        return $link->toUrl();
    }

    public function __toString(): string
    {
        return $this->toUrl();
    }

    /**
     * @return float[]
     */
    protected function getTileCoordinates(): array
    {
        return [
            floor((($this->lat + 180) / 360) * pow(2, $this->zoom)),
            floor((
                    1 -
                    log(
                        tan(deg2rad($this->lon))
                        + 1 / cos(deg2rad($this->lon))
                    ) / pi()
                ) / 2 * pow(2, $this->zoom))
        ];
    }

    /**
     * @return string
     */
    protected function getBoundingBoxQueryValue(): string
    {
        [$xTile, $yTile] = $this->getTileCoordinates();

        // calculate tiles of bounding box
        $tileNW = [
            'x' => ($xTile * static::tileSize - $this->width / 2)
                / static::tileSize,
            'y' => ($yTile * static::tileSize - $this->height / 2)
                / static::tileSize
        ];
        $tileSE = [
            'x' => ($xTile * static::tileSize + $this->width / 2)
                / static::tileSize,
            'y' => ($yTile * static::tileSize + $this->height / 2)
                / static::tileSize
        ];

        \System::log('Zoom: ' . $this->zoom, __METHOD__, TL_GENERAL);

        // transform tile coords to bounding box
        $n = pow(2, $this->zoom);
        $lonLeft = $tileNW['x'] / $n * 360.0 - 180.0;
        $latLeft = rad2deg(atan(sinh(pi() * (1 - 2 * $tileNW['y'] / $n))));
        $lonRight = $tileSE['x'] / $n * 360.0 - 180.0;
        $latRight = rad2deg(atan(sinh(pi() * (1 - 2 * $tileSE['y'] / $n))));

        return sprintf(
            static::queryBoundingBoxPattern,
            $latLeft,
            $lonLeft,
            $latRight,
            $lonRight
        );
    }
}
