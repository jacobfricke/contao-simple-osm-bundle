<?php

namespace Gabelbart\ContaoSimpleOsmBundle;

class ContaoSimpleOsmBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public const ContaoCookiebarClass = '\Oveleon\ContaoCookiebar\ContaoCookiebar';

    public static function contaoCookiebarIsInstalled (): bool
    {
        return class_exists(static::ContaoCookiebarClass);
    }
}
